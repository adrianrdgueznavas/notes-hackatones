const router = require('express').Router()

const {
  getAllNotes,
  createNote,
  updateNote,
  deleteNote

} = require('../controllers/notes.controller')

router.get('/', getAllNotes)
router.post('/', createNote)
router.put('/:noteId', updateNote)
router.delete('/:noteId', deleteNote)

module.exports = router
